<?php

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends AttractorContext
{

    /**
     * @When /^я вижу слово "([^"]*)" где\-то на странице$/
     * @param $arg
     */
    public function ISeeTheWordWhereThatIsOnPage($arg)
    {
        $this->assertPageContainsText($arg);
    }

    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function ImOnTheMainPage()
    {
        $this->visit($this->getContainer()->get('router')->generate('homepage'));
    }

    /**
     * @When /^я заполняю форму регистрации и регистрируюсь$/
     */
    public function IFillInTheRegistrationFormAndRegister()
    {
        $this->fillField('fos_user_registration_form_email', uniqid() . "qwerty@gmail.com");
        $this->fillField('fos_user_registration_form_username', "qwerty");
        $this->fillField('fos_user_registration_form_plainPassword_first', "qwerty");
        $this->fillField('fos_user_registration_form_plainPassword_second', "qwerty");

        $this->pressButton('submit');
    }


    /**
     * @When /^я авторизуюсь с логином "([^"]*)" и паролем "([^"]*)"/
     * @param $login
     * @param $password
     */
    public function ImLoggedInWithALoginAndPassword($login, $password)
    {
        $this->fillField('app_client_username', $login);
        $this->fillField('app_client_password', $password);
        $this->pressButton('app_client_login');
    }

    /**
     * @When /^я кликаю по ссылке "(?P<link>(?:[^"]|\\")*)"$/
     * @param $link
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function IClickOnTheLink($link)
    {
        $link = $this->fixStepArgument($link);
        $this->getSession()->getPage()->clickLink($link);
    }
}

