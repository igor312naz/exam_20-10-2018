<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{

    /**
     * @param ObjectManager $manager
     */
    public function load
    (
        ObjectManager $manager
    )
    {

        $user1 = new User();
        $user1
            ->setEmail('test@gmail.com')
            ->setPlainPassword('qwerty')
            ->setEnabled(true)
            ->setRoles(['ROLE_USER'])
            ->setFullName('Иванов Иван Иванович');
        $this->addReference('user1', $user1);

        $manager->persist($user1);
        $manager->flush();

        $admin = new User();
        $admin
            ->setEmail('admin@gmail.com')
            ->setPlainPassword('123')
            ->setEnabled(true)
            ->setRoles(['ROLE_ADMIN'])
            ->setFullName('Админ ');
        $this->addReference('admin', $admin);


        $manager->persist($admin);
        $manager->flush();
    }



}
