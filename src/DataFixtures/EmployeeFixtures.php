<?php

namespace App\DataFixtures;

use App\Entity\Employee;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class EmployeeFixtures extends Fixture  implements DependentFixtureInterface
{
    public const EMPLOYEE_ONE = 'Еленова Елена';
    public const EMPLOYEE_TWO = 'Сергеев Сергей';
    public const EMPLOYEE_THREE = 'Иванов Иван';

    public function load(ObjectManager $manager)
    {

      /*  $work1=$this->getReference('work1');
        $work2=$this->getReference('work2');
        $work3=$this->getReference('work3');*/


        $employee1 = new Employee();
        $employee1
            ->setName(self::EMPLOYEE_ONE)
            ->setImage('emp1.jpg');
      //      ->setWork($work1);
        $manager->persist($employee1);

        $employee2 = new Employee();
        $employee2
            ->setName(self::EMPLOYEE_TWO)
            ->setImage('emp2.jpg');
     //       ->setWork($work2);
        $manager->persist($employee2);

        $employee3 = new Employee();
        $employee3
            ->setName(self::EMPLOYEE_THREE)
            ->setImage('emp3.jpg');
    //        ->setWork($work3);


        $this->addReference('employee1',$employee1);
        $this->addReference('employee2',$employee2);
        $this->addReference('employee3',$employee3);

        $manager->persist($employee3);



        $manager->flush();

    }

    public function getDependencies()

    {
        return array(

            UserFixtures::class

        );
    }

}
