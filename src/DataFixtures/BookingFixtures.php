<?php

namespace App\DataFixtures;

use App\Entity\Booking;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class BookingFixtures extends Fixture implements DependentFixtureInterface
{


    public function load(ObjectManager $manager)
    {

        $user = $this->getReference('user1');



        $booking1 = new Booking();
        $booking1
            ->setName(WorkFixtures::WORK_ONE)
            ->setAddress('Советская 1')
            ->setNumberOfSquareMeters(5)
            ->setCleaningType('Сухая уборка')
            ->setUser($user)
            ->setStartTime(new \DateTime('2018-10-20 12:00:00'));
        $manager->persist($booking1);

        $booking2 = new Booking();
        $booking2
            ->setName(WorkFixtures::WORK_TWO)
            ->setAddress('Киевская 1')
            ->setUser($user)
            ->setNumberOfSquareMeters(20)
            ->setCleaningType('Влажная уборка')
            ->setStartTime(new \DateTime('2018-10-21 12:00:00'));
        $manager->persist($booking2);

        $booking3 = new Booking();
        $booking3
            ->setName(WorkFixtures::WORK_THREE)
            ->setAddress('Московская 1')
            ->setUser($user)
            ->setNumberOfSquareMeters(20)
            ->setCleaningType('Генеральная уборка')
            ->setStartTime(new \DateTime('2018-10-23 12:00:00'));
        $manager->persist($booking3);

        $this->addReference('booking1', $booking1);
        $this->addReference('booking2', $booking2);
        $this->addReference('booking3', $booking3);



        $manager->flush();

    }


    public function getDependencies()

    {
        return array(

            UserFixtures::class,

        );
    }

}
