<?php

namespace App\DataFixtures;

use App\Entity\Work;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class WorkFixtures extends Fixture implements DependentFixtureInterface
{


    public const WORK_ONE = 'Уборка ванной';
    public const WORK_TWO = 'Уборка дома';
    public const WORK_THREE = 'Уборка квартиры';


    public function load(ObjectManager $manager)
    {


        $booking1 = $this->getReference('booking1');
        $booking2 = $this->getReference('booking2');
        $booking3 = $this->getReference('booking3');

        $employee1= $this->getReference('employee1');
        $employee2 = $this->getReference('employee2');
        $employee3 = $this->getReference('employee3');



        $work1 = new Work();
        $work1

            ->setEmployee($employee1)
            ->setStartTime(new \DateTime('2017-10-10 08:00:00'))
            ->setEndTime(new \DateTime('2017-10-10 14:00:00'))
            ->setBooking($booking1);
        $manager->persist($work1);

        $work2 = new Work();
        $work2

            ->setEmployee($employee2)
            ->setStartTime(new \DateTime('2017-10-10 08:00:00'))
            ->setEndTime(new \DateTime('2017-10-10 14:00:00'))
            ->setBooking($booking2);
        $manager->persist($work2);

        $work3 = new Work();
        $work3
            ->setEmployee($employee3)
            ->setStartTime(new \DateTime('2017-10-10 08:00:00'))
            ->setEndTime(new \DateTime('2017-10-10 14:00:00'))
            ->setBooking($booking3);
        $manager->persist($work3);

        $this->addReference('work1',$work1);
        $this->addReference('work2',$work2);
        $this->addReference('work3',$work3);


        $manager->flush();

    }

    public function getDependencies()

    {
        return array(

            BookingFixtures::class

        );
    }

}
