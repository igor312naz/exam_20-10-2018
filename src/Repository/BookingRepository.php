<?php

namespace App\Repository;

use App\Entity\Booking;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;


/**
 * @method Booking|null find($id, $lockMode = null, $lockVersion = null)
 * @method Booking|null findOneBy(array $criteria, array $orderBy = null)
 * @method Booking[]    findAll()
 * @method Booking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Booking::class);
    }

    public function getBookingsByUser($user)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.user = :user')
            ->setParameter('user', $user)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(100)
            ->getQuery()
            ->getResult();
    }

    public function getSchedule()
    {
        return $this->createQueryBuilder('b')
            ->select('b')
            ->andWhere('b.startTime > CURRENT_TIMESTAMP()')
            ->andWhere('b.startTime < :date')
            ->setParameter('date', new \DateTime('+1 month'))
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(100)
            ->getQuery()
            ->getResult();
    }

}
