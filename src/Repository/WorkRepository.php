<?php

namespace App\Repository;

use App\Entity\Work;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Work|null find($id, $lockMode = null, $lockVersion = null)
 * @method Work|null findOneBy(array $criteria, array $orderBy = null)
 * @method Work[]    findAll()
 * @method Work[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Work::class);
    }

    public function getSessionByDateAndEmployee($startTime, $endTime, $employee)
    {
        return $this->createQueryBuilder('s')
            ->select('s')
            ->innerJoin('s.employee', 'e')
            ->where('s.endTime > :startTime')
            ->setParameter('startTime', $startTime)
            ->andWhere('s.startTime < :endTime')
            ->setParameter('endTime', $endTime)
            ->andWhere('s.employee = :employee')
            ->setParameter('employee', $employee)
            ->orderBy('s.endTime', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
