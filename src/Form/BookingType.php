<?php

namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;



class BookingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,['label' => 'Описание уборки'])
            ->add('address', TextareaType::class,['label' => 'Адресс'])
            ->add('numberOfSquareMeters', NumberType::class,['label' => 'Сколько квадратных метров'])
            ->add('startTime',DateTimeType::class, [
                'label' => 'Время бронирования',
                'years' => range(date('Y'), date('Y') + 5),
                'months' => range(1, 12),
                'days' => range(1, 31),
                'hours' => range(8, 20)
            ])
            ->add('cleaningType', ChoiceType::class, array(
                'label' => 'Тип уборки',
                'choices' => [
                    'Сухая уборка' => '1',
                    'Влажная уборка' => '1.5',
                    'Генеральная уборка' => '2.5'],
                'placeholder' => 'Выбери один из вариантов'
            ))

            ->add('save', SubmitType::class, ['label' => 'Сохранить']);

    }

    public function getBlockPrefix()
    {
        return 'app_booking_add';
    }


}
