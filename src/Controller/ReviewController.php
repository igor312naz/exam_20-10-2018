<?php


namespace App\Controller;

use App\Repository\BookingRepository;
use App\Repository\BookingsRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class ReviewController extends Controller
{
    /**
     * @Route("/review/{id}", name="app_review_booking")
     * @param int $id
     * @param Request $request
     * @param BookingRepository $bookingsRepository
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function reviewBookingAction(int $id, Request $request, BookingRepository $bookingsRepository, ObjectManager $manager)
    {
        $formView = null;
        $booking = $bookingsRepository->find($id);
        if ($booking->getUser() == $this->getUser()) {
            $form = $this->createFormBuilder()
                ->add('review', ChoiceType::class, array(
                    'label' => 'Оцените качество уборки',
                    'choices' => [
                        '1' => '1',
                        '2' => '2',
                        '3' => '3',
                        '4' => '4',
                        '5' => '5'
                    ],
                    'placeholder' => 'Выберите один из вариантов'))
                ->add('isGood', ChoiceType::class, array(
                    'label' => 'Уложились ли в срок?',
                    'choices' => [
                        'Да' => true,
                        'Нет' => false],
                    'placeholder' => 'Выберите один из вариантов'))
                ->add('save', SubmitType::class, array('label' => 'Оценить'))
                ->getForm();
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $formRaw = $form->getData();
                dump($formRaw);
                $booking->setReview($formRaw['review']);
                $booking->setIsGood($formRaw['isGood']);
                $manager->persist($booking);
                $manager->flush();
                $this->addFlash('notice', "Спасибо, за оценку");
                return $this->redirectToRoute('app-booking-show');
            }
            $formView = $form->createView();
        } else {
            $this->addFlash('error', "Ошибка аунтефикации");
        }


        return $this->render('booking.html.twig', [
            'form' => $formView
        ]);
    }


}