<?php


namespace App\Controller;


use App\Entity\Booking;
use App\Entity\Work;
use App\Form\BookingType;
use App\Repository\BookingRepository;
use App\Repository\EmployeeRepository;
use App\Repository\WorkRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;




class IndexController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param BookingRepository $booking
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(BookingRepository $booking){

        $booking=$booking->findAll();


        return $this->render('index.html.twig',[
            'booking'=>$booking
        ]);
    }

    /**
     * @Route("/booking", name="booking")
     * @param Request $request
     * @param WorkRepository $workRepository
     * @param EmployeeRepository $employeesRepository
     * @param ObjectManager $objectManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     * @internal param EmployeesRepository $employeesRepository
     */
    public function createBookingAction( Request $request, WorkRepository $workRepository, EmployeeRepository $employeesRepository, ObjectManager $objectManager)
    {
        if ($this->getUser()){
            $booking = new Booking();

            $booking->setUser($this->getUser());
            $form = $this->createForm(BookingType::class, $booking, array(
                'method' => 'POST'
            ));
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $workHours = $booking->getNumberOfSquareMeters() / 20 * $booking->getCleaningType();
                $amountOfDays = ceil($workHours / 8);
                $cleaningStartTime = new \DateTimeImmutable($booking->getStartTime()->format('Y-m-d H:i:s'));
                $cleaningEndTime = $cleaningStartTime->modify("+{$amountOfDays} days");

                $employees = $employeesRepository->findAll();


                foreach ($employees as $employee) {
                    $sessionOnCurrentDate = $workRepository->getSessionByDateAndEmployee(
                        $cleaningStartTime,
                        $cleaningEndTime,
                        $employee
                    );
                    if ($sessionOnCurrentDate == null) {
                        $hoursUntilDayOff = 20 - (int)$cleaningStartTime->format('H');
                        while ($workHours > 0) {
                            $session = new Work();
                            $session->setEmployee($employee);
                            $session->setBooking($booking);
                            $session->setStartTime($cleaningStartTime);
                            if ($hoursUntilDayOff > 8) {
                                $session->setEndTime($cleaningStartTime->modify('+8 hours'));
                                $workHours -= 8;
                            } else {
                                $session->setEndTime($cleaningStartTime->modify("+{$hoursUntilDayOff} hours"));
                                $workHours -= $hoursUntilDayOff;

                            }
                            $cleaningStartTime = ($cleaningStartTime->modify("+1 days")->setTime(8, 00, 00));
                            $objectManager->persist($booking);
                            $objectManager->persist($session);
                            $objectManager->flush();
                            $hoursUntilDayOff = 9;
                        }
                        break;
                    }
                }


                return $this->redirectToRoute("homepage");
            }
            $showForm = $form->createView();
        } else {
            $showForm = null;
        }


        return $this->render('create_booking.html.twig', [
            'form' => $showForm
        ]);
        }










    /**
     * @Route("/employee", name="employee")
     * @param EmployeeRepository $employee
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function employeeAction(EmployeeRepository $employee)
    {

       $employee = $employee->findAll();
      //  $employee=$employee->getEmployee();


        return $this->render('employee.html.twig', [
            'employee' =>$employee
        ]);


    }


}