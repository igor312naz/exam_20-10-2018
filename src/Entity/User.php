<?php


// src/Entity/User.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    private $newPass;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Booking", mappedBy="user", cascade={"persist", "remove"})
     */
    private $bookings;

    /**
     * Sets the email.
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->setUsername($email);

        return parent::setEmail($email);
    }

    /**
     * Set the canonical email.
     *
     * @param string $emailCanonical
     * @return User
     */
    public function setEmailCanonical($emailCanonical)
    {
        $this->setUsernameCanonical($emailCanonical);

        return parent::setEmailCanonical($emailCanonical);
    }


    /**
     * @var string
     *
     * @ORM\Column(type="string" , length=1024)
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(type="boolean")
     */
    private $unreliable;




    public function __construct()
    {
        parent::__construct();
        $this->unreliable = 0;
        $this->bookings = new ArrayCollection();
    }

    /**
     * @param string $fullName
     * @return User
     */
    public function setFullName(string $fullName)
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }




    public function getNewPass()
    {
        return $this->newPass;
    }

    /**
     * @param mixed $newPass
     * @return User
     */
    public function setNewPass($newPass)
    {
        $this->newPass = $newPass;
        return $this;
    }

    /**
     * @param string $unreliable
     * @return User
     */
    public function setUnreliable(string $unreliable): ?User
    {
        $this->unreliable = $unreliable;
        return $this;
    }

    /**
     * @return string
     */
    public function getUnreliable(): string
    {
        return $this->unreliable;
    }


    /**
     * @param $bookings
     * @return User
     */
    public function setBookings($bookings): ?User
    {
        $this->bookings = $bookings;
        return $this;
    }


    public function getBookings()
    {
        return $this->bookings;
    }


}