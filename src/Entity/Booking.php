<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * BOOKING
 *
 * @ORM\Table(name="booking")
 * @ORM\Entity(repositoryClass="App\Repository\BookingRepository")
 */
class Booking
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * @var string
     *
     * @ORM\Column(name="cleaning_type", type="string", length=255)
     */
    private $cleaningType;


    /**
     * @ORM\Column(type="datetime")
     */
    private $startTime;



    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=2048)
     */
    private $address;


    /**
     * @var integer
     *
     * @ORM\Column(name="number_of_square_meters", type="integer", length=255)
     */
    private $numberOfSquareMeters;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Work", mappedBy="booking", cascade={"persist", "remove"})
     */
    private $work;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $review;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isGood;



    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="bookings")
     */
    private $user;

    public function __construct()
    {
        $this->work = new ArrayCollection();

    }

    /**
     * @param mixed $isGood
     * @return Booking
     */
    public function setIsGood($isGood)
    {
        $this->isGood = $isGood;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getisGood()
    {
        return $this->isGood;
    }

    /**
     * @param mixed $review
     * @return Booking
     */
    public function setReview($review)
    {
        $this->review = $review;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReview()
    {
        return str_repeat("<i class='far fa-star'></i>", $this->review);
        // return $this->review;
    }

    /**
     * @param string $name
     * @return Booking
     */
    public function setName(string $name): Booking
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $cleaningType
     * @return Booking
     */
    public function setCleaningType(string $cleaningType): self
    {
        $this->cleaningType = $cleaningType;
        return $this;
    }

    /**
     * @return string
     */
    public function getCleaningType(): ?string
    {
        return $this->cleaningType;
    }


    public function setStartTime(\DateTimeInterface $startTime): self
    {
        $this->startTime = $startTime;
        return $this;
    }


    public function getStartTime(): ?\DateTimeInterface
    {
        return $this->startTime;
    }

    /**
     * @param string $address
     * @return Booking
     */
    public function setAddress(string $address): Booking
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param int $numberOfSquareMeters
     * @return Booking
     */
    public function setNumberOfSquareMeters(int $numberOfSquareMeters): Booking
    {
        $this->numberOfSquareMeters = $numberOfSquareMeters;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfSquareMeters(): ?int
    {
        return $this->numberOfSquareMeters;
    }



    public function setWork(string $work): self
    {
        $this->work = $work;

        return $this;
    }


    public function getSessions()
    {
        return $this->work;
    }


    /**
     * @param $user
     * @return Booking
     */
    public function setUser($user): ?Booking
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

}

